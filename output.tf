output "asg" {
  value = [ "${module.nat.asg}" ]
}

output "asg_arn" {
  value = [ "${module.nat.asg_arn}" ]
}

output "launch_configuration" {
  value = [ "${module.nat.launch_configuration}" ]
}

output "public_ip_id" {
  value = [ "${module.nat.public_ip_id}" ]
}

output "public_ip" {
  value = [ "${module.nat.public_ip}" ]
}

output "ec2_role" {
  value = [ "${module.nat.ec2_role}" ]
}

output "ec2_role_arn" {
  value = [ "${module.nat.ec2_role_arn}" ]
}

output "ec2_instance_profile" {
  value = "${module.nat.ec2_instance_profile}"
}

output "ec2_instance_profile_arn" {
  value = "${module.nat.ec2_instance_profile_arn}"
}

output "security_group" {
  value = "${module.nat.security_group}"
}
