data "aws_iam_policy_document" "nat_policy_body" {
  statement {
    effect = "Allow"
    actions = [
      "ec2:ModifyInstanceAttribute",
      "ec2:DescribeTags",
      "ec2:AssociateAddress",
      "autoscaling:DescribeTags",
      "ec2:DescribeRouteTables",
      "ec2:CreateRoute",
      "ec2:ReplaceRoute"
    ]

    resources = [ "*" ]
  }
}

resource "aws_iam_policy" "nat_policy" {
  name = "${var.name}"
  path = "/"
  description = "NAT EIP/Route handler access"
  policy = "${data.aws_iam_policy_document.nat_policy_body.json}"
}
