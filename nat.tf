module "nat" {
  source = "git::https://bitbucket.org/credibilit/terraform-nat-ec2-core-blueprint.git?ref=0.0.7"

  account = "${var.account}"
  name = "${var.name}"
  ami = "${var.ami}"
  iam_polices = [
    "${aws_iam_policy.nat_policy.arn}"
  ]
  iam_polices_count = 1
  public_subnets = [
    "${var.public_subnets}"
  ]
  private_routes = [ "${var.private_routes}" ]
  key_pair = "${var.key_pair}"
  additional_security_groups = [ "${var.additional_security_groups}" ]
  user_data = "${file("${path.module}/scripts/user_data.sh")}"
}
