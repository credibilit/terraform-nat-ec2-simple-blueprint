variable "account" {
  description = "The AWS account number ID"
}

variable "region" {
  type = "string"
  description = "The current AWS region"
}

variable "name" {
  type = "string"
  description = "A name for the NAT elements"
}

variable "azs" {
  type = "list"
  description = "List of AZs to deploy the NAT instances"
}

variable "asg_health_check_grace_period" {
  type = "string"
  description = "The "
  default = 60
}

variable "public_subnets" {
  type = "list"
  description = "A list of lists of public subnets to deploy the nat autoscaling groups, one for each NAT"
}

variable "private_routes" {
  type = "list"
  description = "A list of private routes to associate with the NAT instances"
}

variable "instance_type" {
  type = "string"
  description = "The AWS instance family for the NAT instance"
  default = "t2.nano"
}

variable "key_pair" {
  type = "string"
  description = "The key pair name to associate with the SSH service"
}

variable "additional_security_groups" {
  type = "list"
  description = "A list of security groups ids to associate with the NAT instance, this will be merged with the NAT Security Group created by the module"
}

variable "ami" {
  type = "string"
  description = "The AMI to be used on EC2 provisioning"
}

variable "csc_mon" {
  type = "string"
  description = "The CredibiliT tag for monitoring fee"
  default = ""
}

variable "csc_sec" {
  type = "string"
  description = "The CredibiliT tag for security fee"
  default = ""
}

variable "csc_bkp" {
  type = "string"
  description = "The CredibiliT tag for backup fee"
  default = ""
}
