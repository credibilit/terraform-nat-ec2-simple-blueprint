#!/bin/bash

# Update packages and install JSON parser
yum update -y
yum install jq -y

META_DATA_URL='http://169.254.169.254/latest'
REGION_URL="$META_DATA_URL/dynamic/instance-identity/document"
INSTANCE_ID_URL="$META_DATA_URL/meta-data/instance-id"

# Discover the region and instance id
REGION=$(curl $REGION_URL | jq '.region' -r)
INSTANCE_ID=$(curl $INSTANCE_ID_URL)

aws ec2 modify-instance-attribute \
  --region "$REGION" \
  --instance-id "$INSTANCE_ID" \
  --source-dest-check '{"Value": false}'

ASG=$(aws ec2 describe-tags --region $REGION --filters "Name=resource-id,Values=$INSTANCE_ID" 'Name=key,Values=aws:autoscaling:groupName' | jq '.Tags[0].Value' -r)
CONFIG=$(aws autoscaling describe-tags --region $REGION --filter "Name=auto-scaling-group,Values=$ASG" 'Name=Key,Values=AutoHealingConfig' | jq '.Tags[0].Value' -r)

# Associate the EIP for this NAT
EIP_ID=$(echo $CONFIG | jq -r '.eip')
aws ec2 associate-address \
  --region "$REGION" \
  --instance-id "$INSTANCE_ID" \
  --allocation-id "$EIP_ID"


# Associate the Route tables to the NAT instance
ROUTE_TABLES=$(echo $CONFIG | jq -r '.routes' | sed 's/,/ /g')
for route in $ROUTE_TABLES; do
  DEFAULT_ROUTE=$(aws ec2 describe-route-tables \
    --region $REGION \
    --route-table-id $route | \
    jq '.RouteTables[0].Routes[] | select(.DestinationCidrBlock == "0.0.0.0/0") | .DestinationCidrBlock'
  )

  if [[ "x$DEFAULT_ROUTE" == "x" ]]; then
    aws ec2 create-route \
      --region "$REGION" \
      --instance-id "$INSTANCE_ID" \
      --route-table-id "$route" \
      --destination-cidr-block '0.0.0.0/0'
  else
    aws ec2 replace-route \
      --region "$REGION" \
      --instance-id "$INSTANCE_ID" \
      --route-table-id "$route" \
      --destination-cidr-block '0.0.0.0/0'
  fi
done

exit 0
