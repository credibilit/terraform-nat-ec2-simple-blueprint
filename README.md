Generic EC2 based NAT TerraForm Blueprint
===============================

# Use

To create a set of NAT EC2 using autoscaling group with 1/1 with this module you need to insert the following peace of code on your own modules:

```
// Call the module
module "<YOUR_MODULE_NAME>" {
  source = "git::https://bitbucket.org/credibilit/terraform_nat_ec2_simple_blueprint.git?ref=<VERSION>"

  ... <parameters> ...
}
```

Where `<VERSION>` is the desired version of *this* module. The master branch store the list of versions which can be used. The possible parameters are listed in advance on this document.

## TODOs

## Input Parameters

The following parameters are used on this module:


## Output parameters

This are the outputs exposed by this module.
